

# path to directory with maps condition 1 (or differential maps if already prepared)
PATH_MAPS1 = 'sample-data/Dixon_2015-MSC-R1-150k'
# path to directory with maps condition 2 (if above path indicates differential maps than leave below path empty)
PATH_MAPS2 = 'sample-data/Dixon_2015-ESC-R1-150k'
# whether to perform simple normalization of contact maps before subtraction
# recommended to always keep this value as None in which case a constant factor will be set to mean of sums of matrices A and B, one can set this field to float >= 1.0 optionally
NORMALIZE = None
# diagonals of a contact map to be omitted
MASKED_DIAGONALS = [0,1]
# path to file with domains, correct domain format is: [domain_id] chromosome start end
# if domain_id column is missing in you have to change ASSIGN_DOMAIN_ID variable to True
PATH_DOMAINS = 'sample-data/TADs-Armatus-Dixon_2015-MSC-R1-150k'
# whether to convert resolution from bp to bins, if start and end are given in bins than leave this as None, otherwise you have to specify how many bp is 1 bin
CONVERT_RESOLUTION = 150000
# columns separator in domain file
DOMAINS_COL_SEP = ' '
# max number of processes to distribute calculations
NPROCESS = 40
# choose method for differential domains detection, PB is parametric method, permutation is permutation test
METHOD = 0 # 0 - parametric, 1 - permutation
m = ['PB','permutation'][METHOD]
# specify p-value threshold to select differential domains, if you want to output all domains and select it later by yourself than set this to None
PVAL_THRESHOLD = 0.05
# whether to apply multiple hypothesis correction (Bonferroni)
MULTIPLE_TESTING_CORRECTION = True
# directory where to save results
OUT_DIR = '.'



# BELOW IS RELEVANT FOR PERMUTATION METHOD
# number of permuted matrices
PERMUTATIONS = 100
# below is directory with permuted maps to use for testing, if not specified than new maps will be produced
PERMUTATIONS_DIRECTORY = ''
# a file with permuted test statistic distribution to save computations, if not specified than new file will be produced
PERMUTED_TSTAT_FILE = ''
