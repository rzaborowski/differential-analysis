/*
 * Permutation.hpp
 *
 *  Created on: Jun 29, 2016
 *      Author: rafal
 */

#include "Permutation.h"

void permute_diagonal(int k, int N,double** in_mtx, double** out_mtx){
    // rewrite k-th diagonal from in matrix to vector
    std::vector<double> diagonal;
    for(int i=0; i<N-k; i++)
        diagonal.push_back(in_mtx[i][i+k]);
    // permute diagonal
    std::random_shuffle(diagonal.begin(), diagonal.end());
    // rewrite k-th diagonal to out matrix
    int j = 0;
    for(std::vector<double>::iterator it=diagonal.begin(); it != diagonal.end(); ++it){
        out_mtx[j][j+k] = *it;
        j++;
    }
}

void make_calculations(SynchronizedQueue<int>& queue, int N, double** mtx){
    while(1){
        int k = queue.pop();
        if(k == -1)
            break;
        permute_diagonal(k,N,mtx,mtx);
    }
}

void permute(int nthreads, unsigned int random_seed, int N, double* in_mtx, double* out_mtx){
    // rewrite 2d one-block array into 2d array of arrays
    double** mtx = new double*[N];
    for(int i=0; i<N; i++)
        mtx[i] = new double[N];
    int i2d = 0;
    int j2d = 0;
    for(int i=0; i<N*N; i++){
        i2d = i / N;
        j2d = i % N;
        mtx[i2d][j2d] = in_mtx[i];
    }
    //std::srand(std::time(0)); // seed random numbers generator
    std::srand(random_seed); // seed random numbers generator
    // create queue and populate with tasks
    //SynchronizedQueue<int>* queue = new SynchronizedQueue<int>();
    SynchronizedQueue<int> queue;
    for(int i=0; i<N; i++)
        queue.push(i);
    for(int i=0; i<nthreads; i++)
    	queue.push(-1);
    // start workers
    std::vector<std::thread> threads;
    for(int i=0; i<nthreads; i++)
        threads.push_back( std::thread(make_calculations,std::ref(queue),N,mtx) );
    for (auto& th : threads)
    	th.join();
    // rewrite results to 2d one-block array out_mtx
    int counter = 0;
    for(int i=0; i<N; i++){
    	counter += i;
        for(int j=i; j<N; j++){
            out_mtx[counter] = mtx[i][j];
            counter++;
        }
    }
    // destroy
    //delete queue;
    for(int i=0; i<N; i++)
        delete mtx[i];
    delete mtx;
}

extern "C"{
    void permute_mtx(int nthreads, unsigned int random_seed, int N, double* in_mtx, double* out_mtx){ permute(nthreads,random_seed,N,in_mtx,out_mtx); }
}
