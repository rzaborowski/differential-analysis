/*
 * SynchronizedQueue.hpp
 *
 *  Created on: Jun 29, 2016
 *      Author: rafal
 */

#include "SynchronizedQueue.h"

template <typename T>
SynchronizedQueue<T>::~SynchronizedQueue() {
	// TODO Auto-generated destructor stub
}

template <typename T>
T SynchronizedQueue<T>::pop()
{
	std::unique_lock<std::mutex> mlock(mutex_);
	while(queue_.empty())
	{
		cond_.wait(mlock);
	}
	auto item = queue_.front();
	queue_.pop();
	return item;
}

template <typename T>
void SynchronizedQueue<T>::pop(T& item)
{
	std::unique_lock<std::mutex> mlock(mutex_);
	while(queue_.empty())
	{
		cond_.wait(mlock);
	}
	item = queue_.front();
	queue_.pop();
}

template <typename T>
void SynchronizedQueue<T>::push(const T& item)
{
	std::unique_lock<std::mutex> mlock(mutex_);
	queue_.push(item);
	mlock.unlock();
	cond_.notify_one();
}

template <typename T>
void SynchronizedQueue<T>::push(T&& item)
{
	std::unique_lock<std::mutex> mlock(mutex_);
	queue_.push(std::move(item));
	mlock.unlock();
	cond_.notify_one();
}
