/*
 * SynchronizedQueue.h
 *
 *  Created on: Jun 29, 2016
 *      Author: rafal
 */

#ifndef SYNCHRONIZEDQUEUE_H_
#define SYNCHRONIZEDQUEUE_H_

#include <mutex>
#include <condition_variable>
#include <queue>

template <typename T>
class SynchronizedQueue {
	std::queue<T> queue_;
	std::mutex mutex_;
	std::condition_variable cond_;
public:
	~SynchronizedQueue();

	T pop();
	void pop(T& item);
	void push(const T& item);
	void push(T&& item);
};

#include "SynchronizedQueue.hpp"

#endif /* SYNCHRONIZEDQUEUE_H_ */
