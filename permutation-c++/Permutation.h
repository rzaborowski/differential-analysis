/*
 * Permutation.h
 *
 *  Created on: Jun 29, 2016
 *      Author: rafal
 */

#ifndef PERMUTATION_H_
#define PERMUTATION_H_

#include <vector>
#include <algorithm>
#include <thread>
#include "SynchronizedQueue.h"

void permute_diagonal(int, int, double**, double**);
void make_calculations(SynchronizedQueue<int>&, int, double**);
void permute(int, unsigned int, int,double*,double*);

#endif /* PERMUTATION_H_ */
