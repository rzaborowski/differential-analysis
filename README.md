# HI-C DIFFERENTIAL ANALYSIS #
------------------------------

## Quick summary ##
-------------------

HiC differential analysis is a pipeline written in python and c++ designed for detection of differential interactions. Given 2 different Hi-C maps and a collection of TADs from the same chromosome we are often interested, which TADs are enriched or depleted in interactions. This of course requires some measure of enrichment or depletion and background model to compare observed effect with. In our pipeline 2 simple methods with different measures and background models are available:  

* permutation test
* parametric test

For details please see [here](doc/methods.pdf).

## Prerequisites ##
-------------------

As mentioned above the code is written in python and c++, however some python snippets use R functions, so one should have also R installed. Below are listed prerequisites (versions for which tests were performed are given in parenthesis):

* GCC (version 5.3.0)  
* R (version 3.2.3)  
* python (version 2.7.11)

python libraries:

* numpy (version 1.10.4)  
* rpy2 (version 2.7.8)  
* statsmodels (version 0.6.1)
* matplotlib (version 1.5.1) for optional plotting script

## Configuration ##
-------------------

After installing prerequisites clone this repository, enter it and hit make to compile additional code. Pipeline is ready to use.

## Usage ##
-----------

To run pipeline open command line, enter directory with this README and execute:
```sh
python analyze.py
```
Pipeline has several options to configure before usage including method selection. All options are configured via CONFIGURE.py file.

### Input ###

* collection of contact maps from experiment 1
* collection of contact maps from experiment 2
* collection of TADs

#### Input file format ####

Directory with contact maps must contain files with names matching following pattern: mtx-?-?.npy where ? is chromosome number. Files must be matrices in numpy format. Note that for differential analysis both directories must contain the same set of chromosomes. Eventually a subset of chromosomes present in both directories will be selected.

TADs file need to have following format:
TADid TADchromosome TADstart TADend
First line is header, remaining lines are domains. All columns must be integers. TADid must be unique values for each TAD inside file. TAD chromosome must much chromosomes from contact maps directories. TADstart and TADend can be expressed in either basepairs or bins. This can be specified in CONFIG file via CONVERT_RESOLUTION field.

#### Sample data ####

Directory sample-data contains example contact maps (taken from: http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE52457 and normalized using iterative correction approach: http://mirnylab.bitbucket.org/hiclib/) in numpy format and TADs (produced using Armatus software: https://www.cs.cmu.edu/~ckingsf/software/armatus/). Have a look at contact maps file namings and content of TADs files. The directory with sample data can be downloaded by executing:
```sh
wget http://regulomics.mimuw.edu.pl/~rafal/differential-pipeline/sample-data.tar.gz
```
### Output ###

Subset of TADs with assigned test statistic, p-values and type (enrichment or depletion) in text file. The format is following:
TADid TADchromosome TADstart TADend tstat pvalue TADtype

### General options ###

* PATH_MAPS - string specifying path to directory with contact maps for experiment 1,
* PATH_MAPS2 - string specifying path to directory with contact maps for experiment 2,
* NORMALIZE - constant factor for normalization of contact maps before subtraction, default to None sets constant factor as mean of sums of both contact maps, optionally one may set this as float >= 1.0 (when equals to 1.0 no normalization is performed before subtraction),
* MASKED_DIAGONALS - sometimes first one or two diagonals are excluded from contact maps as they don't bring important information, this field allow to reject diagonals by specifying them as python list, for example [0,1] will exclude 0-th and 1-st diagonal,
* PATH_DOMAINS - string specifying path to file with TADs,
* CONVERT_RESOLUTION - integer specifying contact maps resolution in base pairs to convert TAD coordinates from basepairs to bins, if TAD coordinates (i.e. TADstart and TADend) are expressed in bins instead of basepairs than this filed must be set to None,
* DOMAINS_COL_SEP - column separator in TADs file,
* NPROCESS - maximum number of process for concurrent mode
* METHOD - field specifying, which method to use for differential TADs detection, 0 - parametric, 1 - permutation,
* PVAL_THRESHOLD - p-value threshold to determine differential TADs, to keep all TADs, set this field to None,
* MULTIPLE_HYPOTHESIS_CORRECTION - whether to apply Bonfferroni correction to determine differential TADs, either True or False,
* OUT_DIR - path to directory where results should be saved, default to current directory, if given but directory don't exist than it will be created.

### Options relevant to permutation method ###

* PERMUTATIONS - number of permutations to produce random distribution of test statistic (median of TAD interactions difference),
* PERMUTATIONS_DIRECTORY - this field specifies whether to save/load permuted contact maps, if empty string then do not save permuted maps, if given string is a path to existing directory with permuted maps files (for example produced during previous run of a pipeline with save permuted maps options) this will load permuted maps instead of computing them (note that in this case the number of files in directory must match PERMUTATIONS field), if string is a path to empty directory or directory don't exists then this will save permuted maps to specified directory,
* PERMUTED_TSTAT_FILE - path to file with distribution of random test statistic for each TAD (numpy npz file), if empty or non existing then distribution will be calculated and saved to permuted-tstat-median-distribution.npz or specified filename respectively.

## Visualization ##
-------------------

Pipeline also contain plotting script, plot.py. It allows to plot contact maps/differential maps with/without TADs in different ways. For example it is possible to draw:

* 2 differential maps or contact maps (one on upper triangle, the other on the lower triangle) with/without TADs, note that both (upper and lower TADs) have coordinates in bp not in bins in their files and therefore both --hic-resolution-upper and --hic-resolution-lower flags must be specified:
```sh
python plot.py -c 7 -l both --upper1=sample-data/Dixon_2015-MSC-R1-150k --upper2=sample-data/Dixon_2015-ESC-R1-150k --lower1=sample-data/Dixon_2015-MSC-R1-150k --lower2=sample-data/Dixon_2015-MDC-R1-150k --tad_upper=sample-data/TADs-Armatus-Dixon_2015-ESC-R1-150k --hic-resolution-upper=150000 --tad_lower=sample-data/TADs-Armatus-Dixon_2015-MDC-R1-150k --hic-resolution-lower=150000 
```
* 1 differential map and 1 contact map with/without TADs,:
```sh
python plot.py -c 7 -l both --upper1=sample-data/Dixon_2015-MSC-R1-150k --upper2=sample-data/Dixon_2015-ESC-R1-150k --lower1=sample-data/Dixon_2015-MSC-R1-150k --tad_lower=sample-data/TADs-Armatus-Dixon_2015-MDC-R1-150k --hic-resolution-lower=150000
```
* 1 differential map or 1 contact map with/without TADs:
```sh
python plot.py -c 7 -l both --upper1=sample-data/Dixon_2015-MSC-R1-150k --tad_lower=sample-data/TADs-Armatus-Dixon_2015-MDC-R1-150k --hic-resolution-lower=150000
```
* the same as above, but now passing full path to files with contact maps instead of directory containing them (in this case contact map file name don't have to follow mtx-?-?.npy pattern):
```sh
python plot.py -c 7 -l both --upper1=sample-data/Dixon_2015-MSC-R1-150k/mtx-7-7.npy --tad_lower=sample-data/TADs-Armatus-Dixon_2015-MDC-R1-150k --hic-resolution-lower=150000
```
* additionally one can also zoomin selected region, specifing zoom magnitude and x and y interval:
```sh
python plot.py -c 7 -l both --upper1=sample-data/Dixon_2015-MSC-R1-150k/mtx-7-7.npy --tad_lower=sample-data/TADs-Armatus-Dixon_2015-MDC-R1-150k --hic-resolution-lower=150000 --zoomin=4 --zoomin_xstart=200 --zoomin_xend=400 --zoomin_ystart=200 --zoomin_yend=400
```

Mandatory options are:

* at least 1 directory with contact maps (-upper1),
* chromosome (-c),
* if one wants to plot TADs and their coordinates are expressed in basepairs rather than bins, --hic-resolution field must be specified to convert TADs coordinates from basepairs to bins,
* contact maps can be specified either as directory containing files with required naming pattern, in which case appropriate contact map files will be parsed based on specified chromosome or user can specify full path to a file with numpy contact map wich then do not need to follow any file name pattern. 

To see available options run:
```sh
python plot.py --help.
```
