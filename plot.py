import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import numpy as np
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid1.inset_locator import inset_axes, BboxConnector, BboxPatch
from matplotlib.transforms import TransformedBbox

from matplotlib.colors import LogNorm, SymLogNorm
from collections import defaultdict
from argparse import ArgumentParser
import warnings
import os
import sys
import math


class Matrix(object):

    def __init__(self,mtx,mask=None):
        if mask is None:
            m = np.zeros(mtx.shape)
        else:
            m = mask
        self.mtx = np.ma.array(mtx,mask=m)
        self.props = {'type' : 'contact', 'min' : mtx.min(), 'max' : mtx.max(), 'empty' : False if self.mtx.sum() else True}

    def __sub__(self,other):
        assert self.mtx.shape == other.mtx.shape
        assert np.array_equal(self.mtx.mask,other.mtx.mask)
        mtx = Matrix(self.mtx - other.mtx,mask=self.mtx.mask)
        mtx.props['type'] = 'differential'
        return mtx

    def __len__(self):
        return len(self.mtx)

    def mask(self,which='none'):
        assert which in ['none','upper','lower','all']
        mask = np.zeros(self.mtx.shape)
        i,j = np.indices(self.mtx.shape)
        if which == 'upper':
            mask[j <= i] = 1.0
        elif which == 'lower':
            mask[i <= j] = 1.0
        elif which == 'all':
            mask = np.ones(self.mtx.shape)
        self.mtx.mask = mask

    def sum(self):
        return self.mtx.sum()

    def normalize(self,factor=1.0):
        self.mtx = self.mtx * factor / self.mtx.sum()

class TADS(object):

    def _lines(self,path,header=True,sep=' '):
        with open(path,'r') as handle:
            if header:
                handle.next()
            for line in handle:
                yield line.strip().split()

    def __init__(self,path,chromosome,hic_res=1,category=""):
        self.tads = defaultdict(list)
        if path:
            gen = self._lines(path,header=False)
            header = gen.next()
            if category and category in header:
                i = header.index(category)
            else:
                i = None
            name = os.path.splitext(os.path.split(path)[1])[0]
            for l in gen:
                if l[1] == chromosome:
                    if i is not None:
                        self.tads[l[i]].append([int(l[2]) / hic_res, int(l[3]) / hic_res])
                    else:
                        self.tads[name].append([int(l[2]) / hic_res, int(l[3]) / hic_res])

    def __getitem__(self, key):
        return self.tads[key]

    def __setitem__(self, key, value):
        # key is id, value is chromosome, start, end
        self.tads[key] = value

    def __iter__(self):
        return iter(self.tads)

    def __len__(self):
        return len(self.tads)

    def keys(self):
        return self.tads.keys()

class Model(object):

    def _get_map(self,path,c):
        if os.path.isfile(path):
            return np.load(path)
        return np.load('{0}/mtx-{1}-{1}.npy'.format(path,c))

    def __init__(self,**kwargs):
        # parse maps
        self.upper = Matrix(self._get_map(kwargs["upper1"],kwargs["chromosome"]))
        if kwargs["upper2"]:
            upper2 = Matrix(self._get_map(kwargs["upper2"],kwargs["chromosome"]))
            if kwargs["normalize"] in ['upper','both']:
                f = np.mean([self.upper.sum(),upper2.sum()])
                self.upper.normalize(factor=f)
                upper2.normalize(factor=f)
            self.upper -= upper2
        self.upper.mask(which="lower")
        if kwargs["lower1"] and kwargs["lower2"]:
            lower1 = Matrix(self._get_map(kwargs["lower1"],kwargs["chromosome"]))
            lower2 = Matrix(self._get_map(kwargs["lower2"],kwargs["chromosome"]))
            if kwargs["normalize"] in ['lower','both']:
                f = np.mean([lower1.sum(),lower2.sum()])
                lower1.normalize(factor=f)
                lower2.normalize(factor=f)
            self.lower = lower1 - lower2
            self.lower.mask(which="upper")
        elif kwargs["lower1"]:
            self.lower = Matrix(self._get_map(kwargs["lower1"],kwargs["chromosome"]))
            self.lower.mask(which="upper")
        elif kwargs["lower2"]:
            self.lower = Matrix(self._get_map(kwargs["lower2"],kwargs["chromosome"]))
            self.lower.mask(which="upper")
        else:
            self.lower = Matrix(np.zeros(self.upper.mtx.shape))
            self.lower.mask(which="all")
            self.upper.mask(which='none')
        # parse TADs
        self.tads_upper = TADS(kwargs["tads_upper"],kwargs["chromosome"],hic_res=kwargs["hic_res_up"],category=kwargs["cat_up"])
        self.tads_lower = TADS(kwargs["tads_lower"],kwargs["chromosome"],hic_res=kwargs["hic_res_low"],category=kwargs["cat_low"])

    def get_levels(self):
        return set(self.tads_lower.keys() + self.tads_upper.keys())

class View(object):

    def __init__(self,**kwargs):
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(1,1,1)
        self.divider = make_axes_locatable(self.ax)
        if kwargs:
            self.coords = [kwargs['xs'],kwargs['xe'],kwargs['ys'],kwargs['ye']]
            if kwargs['ys'] >= kwargs['xe']:
                self.loc = 'left'
            elif kwargs['ye'] <= kwargs['xs']:
                self.loc = 'right'
            elif kwargs['xs'] <= kwargs['length'] - kwargs['xe']:
                self.loc = 'left'
            else:
                self.loc = 'right'
            if self.loc == 'left':
                self.axins = inset_axes(self.ax,kwargs['zoomin'],kwargs['zoomin'],loc=4,bbox_transform=self.ax.transAxes,bbox_to_anchor=(-0.15,0.0))
            else:
                self.axins = inset_axes(self.ax,kwargs['zoomin'],kwargs['zoomin'],loc=3,bbox_transform=self.ax.transAxes,bbox_to_anchor=(1.25,0.0))
        else:
            self.loc = 'right'
            self.axins = None

    def plot_map(self,matrix,cmcolor,log_scale,colorbar):
        mx = max(np.abs([matrix.props["min"],matrix.props["max"]]))
        if log_scale:
            if matrix.props['type'] == 'differential':
                ls = SymLogNorm(1.0,vmin=-mx,vmax=mx)
            else:
                ls = LogNorm()
        else:
            ls=None
        im = self.ax.imshow(matrix.mtx,
                interpolation='nearest',
                cmap=cmcolor[matrix.props['type']],
                norm=ls,
                origin='lower',
                extent=[0,len(matrix),0,len(matrix)])
        if self.axins is not None:
            self.axins.imshow(matrix.mtx,
                interpolation='nearest',
                cmap=cmcolor[matrix.props['type']],
                norm=ls,
                origin='lower',
                extent=[0,len(matrix),0,len(matrix)])
        assert colorbar in ['none','left','right','top','bottom']
        if colorbar != 'none' and not matrix.props["empty"]:
            vh_orient = 'vertical' if colorbar in ['left','right'] else 'horizontal'
            cax = self.divider.append_axes(colorbar, size="5%", pad=0.05)
            cb = self.fig.colorbar(im,orientation=vh_orient,cax=cax)
            if matrix.props['type'] == 'differential' and log_scale:
                n = int(math.ceil(math.log10(mx)))
                cb.set_ticks([-float(10 ** k) for k in range(n,-1,-1)]+[0.0]+[float(10 ** k) for k in range(0,n+1)])
            if colorbar == 'top':
                cb.ax.xaxis.set_ticks_position(colorbar)
            cb.ax.zorder = -1

    def plot_tads(self,tads,which,tads_colormap='gist_rainbow',color_mapping={}):
        mapping = color_mapping
        if tads.keys():
            # reverse sort tads levels according to tad length for better rendering
            sorter = [(k,np.mean([e-s for s,e in v])) for k,v in tads.tads.iteritems()]
            sorter.sort(key=lambda x: x[1],reverse=True)
            cats = zip(*sorter)[0]
            if set(cats).issubset(set(color_mapping.keys())):
                mapping = color_mapping
            else:
                cm = plt.get_cmap(tads_colormap)
                if len(cats) == 1:
                    mapping = {cats[0] : cm(0.5)}
                else:
                    mapping = dict(zip(cats,[cm(1.*i/(len(cats)-1)) for i in range(len(cats))]))
            for level in cats:
                color = mapping[level]
                for s,e in tads[level]:
                    if which == 'lower':
                        self.ax.plot([s,e],[s,s],color=color,linewidth=1.0)
                        self.ax.plot([e,e],[s,e],color=color,linewidth=1.0)
                        if self.axins is not None:
                            self.axins.plot([s,e],[s,s],color=color,linewidth=2.0)
                            self.axins.plot([e,e],[s,e],color=color,linewidth=2.0)
                    else:
                        self.ax.plot([s,s],[s,e],color=color,linewidth=1.0)
                        self.ax.plot([s,e],[e,e],color=color,linewidth=1.0)
                        if self.axins is not None:
                            self.axins.plot([s,s],[s,e],color=color,linewidth=2.0)
                            self.axins.plot([s,e],[e,e],color=color,linewidth=2.0)
        return mapping

    def render_zoomin(self):
        if self.axins is not None:
            xs,xe,ys,ye = self.coords
            if self.loc == 'left':
                l1a, l1b, l2a, l2b = 1, 4, 2, 3
            else:
                l1a, l1b, l2a, l2b = 2, 3, 1, 4
            self.axins.set_xlim(xs,xe)
            self.axins.set_ylim(ys,ye)
            self.axins.set_xticklabels(map(int,self.axins.get_xticks()),rotation=45)
            self.axins.tick_params(direction='out',top='off',right='off',labelbottom='on',labeltop='off',labelsize=7)
            # self defined mark_inset to conect different corners
            kwargs = {'fc' : "none", 'ec' : 'k', 'zorder' : 3}
            rect = TransformedBbox(self.axins.viewLim, self.ax.transData)
            pp = BboxPatch(rect,**kwargs)
            self.ax.add_patch(pp)
            p1 = BboxConnector(self.axins.bbox, rect, loc1=l1a, loc2=l2a, **kwargs)
            self.axins.add_patch(p1)
            p1.set_clip_on(False)
            p2 = BboxConnector(self.axins.bbox, rect, loc1=l1b, loc2=l2b, **kwargs)
            self.axins.add_patch(p2)
            p2.set_clip_on(False)

    def plot_legend(self,mapping,max_row_number=15):
        m = {}
        for k,v in mapping.iteritems():
            try:
                m[int(k)] = v
            except ValueError:
                m[k] = v
        patches = [mpatches.Patch(color=m[k],label=k) for k in sorted(m.keys())]
        col_number = len(patches) / max_row_number
        if len(patches) % max_row_number:
            col_number += 1
        if self.loc == 'left':
            self.ax.legend(handles=patches,loc='lower left',bbox_to_anchor=(1.2,0.0),fancybox=True,shadow=True,ncol=col_number)
        else:
            self.ax.legend(patches,[p.get_label() for p in patches],loc='lower right',bbox_to_anchor=(-0.2,0.0),fancybox=True,shadow=True,ncol=col_number)

    def limit_area(self,coords):
        xs,xe,ys,ye = coords
        self.ax.set_xlim(xs,xe)
        self.ax.set_ylim(ys,ye)

class Controller(object):

    def _check_coords(self,coords):
        correct = True
        if sum(coords) <= 0:
            correct = False
            warnings.warn("zoomin specified, but no fragment to zoomin was specified! Skipping zoomin ...")
        if any(i < 0 for i in coords):
            correct = False
            warnings.warn("zoomin specified, but negative coordinates given for fragment selection! Skipping zoomin ...")
        if coords[0] == coords[1] and coords[2] == coords[3]:
            correct = False
            warnings.warn("zoomin specified, but all x_start == x_end and y_start == y_end! Skipping zoomin ...")
        if coords[0] > coords[1] or coords[2] > coords[3]:
            correct = False
            warnings.warn("zoomin specified, but all x_start > x_end or y_start > y_end! Skipping zoomin ...")
        return correct

    def _correct_coords(self,length,coords):
        if coords[0] == coords[1]:
            cc = [0,length,coords[2],coords[3]]
        elif coords[2] == coords[3]:
            cc = [coords[0],coords[1],0,length]
        else:
            cc = coords
        if cc[1] > length:
            cc[1] = length-1
        if cc[3] > length:
            cc[3] = length-1
        return cc

    def __init__(self,options):
        ############################# Model #############################
        model = Model(upper1=options.cme1u,
                upper2=options.cme2u,
                normalize=options.norm,
                lower1=options.cme1l,
                lower2=options.cme2l,
                tads_upper=options.du,
                chromosome=options.c,
                hic_res_up=options.hicresu,
                cat_up=options.duc,
                tads_lower=options.dl,
                hic_res_low=options.hicresl,
                cat_low=options.dlc)
        ############################# View #############################
        length = len(model.upper)
        coords1 = [options.xs,options.xe,options.ys,options.ye]
        correct1 = False
        if sum(coords1):
            correct1 = self._check_coords(coords1)
        if correct1:
            coords1 = self._correct_coords(length,coords1)
        correct2 = False
        if options.zi > 0:
            coords2 = [options.zixs,options.zixe,options.ziys,options.ziye]
            correct2 = self._check_coords(coords2)
            if correct2:
                coords2 = self._correct_coords(length,coords2)
                if correct1:
                    correct2 = (coords1[0] < coords2[0] < coords1[1]) and \
                            (coords1[0] < coords2[1] < coords1[1]) and\
                            (coords1[2] < coords2[2] < coords1[3]) and\
                            (coords1[2] < coords2[3] < coords1[3])
                    if not correct2:
                        warnings.warn("Specified main zoom area and extra zoom in, but extra zoom area is larger than main zoom! Omitting extra zoom...")
        if correct2:
            kwargs = {'zoomin' : options.zi, 'xs' : coords2[0], 'xe' : coords2[1],
                    'ys' : coords2[2], 'ye' : coords2[3], 'length' : length}
            view = View(**kwargs)
        else:
            view = View()
        ############################# Controller #############################
        view.plot_map(model.upper,
                {'contact' : options.cmapcolor, 'differential' : options.diffcolor},
                True if (options.l == 'both' or options.l == 'upper') else False,
                'top' if options.cb else 'none')
        view.plot_map(model.lower,
                {'contact' : options.cmapcolor, 'differential' : options.diffcolor},
                True if (options.l == 'both' or options.l == 'lower') else False,
                'right' if options.cb else 'none')
        if correct1:
            view.limit_area(coords1)
        view.ax.set_xticklabels(map(int,view.ax.get_xticks()),rotation=45)
        view.ax.autoscale(False)
        view.ax.tick_params(direction='out',top='off',right='off',labelbottom='on',labeltop='off')
        levels = model.get_levels()
        cm = plt.get_cmap(options.tcm)
        mapping = {j : cm(1.*i/len(levels)) for i,j in enumerate(levels)}
        view.plot_tads(model.tads_upper,'upper',tads_colormap=options.tcm,color_mapping=mapping)
        view.plot_tads(model.tads_lower,'lower',tads_colormap=options.tcm,color_mapping=mapping)
        if correct2:
            view.render_zoomin()
        view.ax.plot([0,length],[0,length],color='k',linewidth=1.0)
        if view.axins is not None:
            view.axins.plot([0,length],[0,length],color='k',linewidth=2.0)
        # legend
        if mapping:
            view.plot_legend(mapping)
        view.fig.savefig('map-{0}-{0}.pdf'.format(options.c),dpi=options.r,bbox_inches='tight')

if __name__=="__main__":
    parser = ArgumentParser()
    parser.add_argument("--upper1",dest="cme1u",required=True,
            help="directory with contact maps (or path to single numpy file) for experiment 1 to be plotted on upper triangle of a map")
    parser.add_argument("-c","--chromosome",dest="c",required=True,
            help="chromosome for which a map is to be plotted")
    parser.add_argument("--upper2",dest="cme2u",default="",
            help="directory with contact maps (or path to single numpy file) for experiment 2 to be plotted on upper triangle of a map")
    parser.add_argument("--lower1",dest="cme1l",default="",
            help="directory with contact maps (or path to single numpy file) for experiment 1 to be plotted on lower triangle of a map")
    parser.add_argument("--lower2",dest="cme2l",default="",
            help="directory with contact maps (or path to single numpy file) for experiment 2 to be plotted on lower triangle of a map")
    parser.add_argument("--normalize",dest="norm",default="both",
            help="which differential maps should be normalized.",metavar="[none|upper|lower|both]")
    parser.add_argument("--tad_upper",dest="du",default="",
            help="file with TADs to be plotted on upper triangle of a map.")
    parser.add_argument("--tad_lower",dest="dl",default="",
            help="file with TADs to be plotted on lower triangle of a map.")
    parser.add_argument("-l","--logarithmic_scale",dest="l",default="none",choices=['none','upper','lower','both'],
            help="which part of map to plot in logarithmic scale",metavar="[none|upper|lower|both]")
    parser.add_argument("-n","--name",dest="n",default="diff-mtx",
            help="filename for resulting image")
    parser.add_argument("-r","--resolution",dest="r",type=int,default=300,
            help="resolution of resulting image in dpi")
    parser.add_argument("-b","--colorbar",dest="cb",default=True,
            help="whether to plot colorbars")
    parser.add_argument("--hic-resolution-upper",dest="hicresu",type=int,default=1,
            help="hi-c resolution in basepairs to convert upper TADs coordinates from bp to bins, if TADs coordinates are in bins already set this to 1 (default)")
    parser.add_argument("--hic-resolution-lower",dest="hicresl",type=int,default=1,
            help="hi-c resolution in basepairs to convert lower TADs coordinates from bp to bins, if TADs coordinates are in bins already set this to 1 (default)")
    parser.add_argument("--diff-colormap",dest="diffcolor",default='seismic',
            help="colormap for differential heatmaps (check matplotlib colormaps for available colors: http://matplotlib.org/examples/color/colormaps_reference.html)")
    parser.add_argument("--cmap-colormap",dest="cmapcolor",default='Reds',
            help="colormap for contact map heatmaps (check matplotlib colormaps for available colors: http://matplotlib.org/examples/color/colormaps_reference.html)")
    parser.add_argument("--categorize_upper_tads",dest="duc",default="",
            help="this can be one of the columns in TADs file of string type, like enriched, depleted, level, etc. If specified TADs will be categorized by specified column and different TAD categories will have different colors")
    parser.add_argument("--categorize_lower_tads",dest="dlc",default="",
            help="this can be one of the columns in TADs file of string type, like enriched, depleted, level, etc. If specified TADs will be categorized by specified column and different TAD categories will have different colors.")
    parser.add_argument("--tads_colormap",dest="tcm",default='gist_rainbow',
            help="colormap to plot different TAD categories if categorize TADs specified (check matplotlib colormaps for available colors: http://matplotlib.org/examples/color/colormaps_reference.html)")
    parser.add_argument("--xstart",dest="xs",type=int,default=0,
            help="x start coordinate to limit matrix area.")
    parser.add_argument("--xend",dest="xe",type=int,default=0,
            help="x end coordinate to limit matrix area.")
    parser.add_argument("--ystart",dest="ys",type=int,default=0,
            help="y start coordinate to limit matrix area.")
    parser.add_argument("--yend",dest="ye",type=int,default=0,
            help="y end coordinate to limit matrix area.")
    parser.add_argument("--zoomin",dest="zi",type=float,default=0,
            help="zoomin given fragment of a contact map.")
    parser.add_argument("--zoomin_xstart",dest="zixs",type=int,default=0,
            help="x start coordinate of a zoom.")
    parser.add_argument("--zoomin_xend",dest="zixe",type=int,default=0,
            help="x end coordinate of a zoom.")
    parser.add_argument("--zoomin_ystart",dest="ziys",type=int,default=0,
            help="y start coordinate of a zoom.")
    parser.add_argument("--zoomin_yend",dest="ziye",type=int,default=0,
            help="y end coordinate of a zoom.")

    ############################### END OF ARG PARSER ###############################

    args = parser.parse_args()
    if len(sys.argv[1:]) == 0:
        parser.print_help()
        sys.exit(1)

    Controller(args)
