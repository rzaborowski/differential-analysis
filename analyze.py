import numpy as np
from multiprocessing import Process, JoinableQueue
from collections import defaultdict
from random import randrange
import re
import os
import warnings
import ctypes
import sys
import tempfile
import shutil

from statsmodels.sandbox.stats.multicomp import multipletests
from rpy2 import robjects

import CONFIG as cnf


class MtxWrapper(object):

    def __symmetric(self,mtx):
        return (mtx.transpose() == mtx).all()

    def __init__(self,**kwargs):
        # one can either pass path to npy file or loaded numpy matrix
        if 'path' in kwargs:
            self.mtx = np.load(kwargs['path'])
        else:
            self.mtx = kwargs['mtx']
        assert self.__symmetric(self.mtx) , "Matrix is not symmetric!!!"
        self.masked_diagonals = kwargs['masked_diagonals'] if 'masked_diagonals' in kwargs else []
        self.__idx_unmappable = {}
        # python wrapper for c++ code
        self.__lib = ctypes.cdll.LoadLibrary('./permutation-c++/Permutation.so')
        self.__fun = self.__lib.permute_mtx
        self.__fun.restype = None
        self.__fun.argtypes = [ctypes.c_int,ctypes.c_uint,ctypes.c_int,
                np.ctypeslib.ndpointer(ctypes.c_double),np.ctypeslib.ndpointer(ctypes.c_double)]

    def __add__(self,other):
        assert self.masked_diagonals == other.masked_diagonals , "Matrices have different diagonals masked!"
        self.restore_unmappable()
        other.restore_unmappable()
        return MtxWrapper(mtx=self.mtx+other.mtx,masked_diagonals=self.masked_diagonals)

    def __sub__(self,other):
        assert self.masked_diagonals == other.masked_diagonals , "Matrices have different diagonals masked!"
        self.restore_unmappable()
        other.restore_unmappable()
        return MtxWrapper(mtx=self.mtx-other.mtx,masked_diagonals=self.masked_diagonals)

    def sum(self):
        return self.mtx.sum()

    def normalize(self,constant=10**7):
        self.mtx = self.mtx * constant / self.mtx.sum()

    def remove_unmappable(self):
        if self.__idx_unmappable:
            return
        self.__idx_unmappable = {i for i,v in enumerate(self.mtx) if sum(v) == 0}
        self.mtx = np.delete(self.mtx,list(self.__idx_unmappable),0)
        self.mtx = np.delete(self.mtx,list(self.__idx_unmappable),1)

    def restore_unmappable(self):
        if not self.__idx_unmappable:
            return
        n = len(self.mtx) + len(self.__idx_unmappable)
        idx_mappable = np.array([i for i in range(n) if i not in self.__idx_unmappable])
        m = np.zeros((n,n))
        m[idx_mappable.reshape(-1,1),idx_mappable] = self.mtx
        self.mtx = m
        self.__idx_unmappable = {}

    def sym2triu(self):
        self.mtx = np.triu(self.mtx)

    def triu2sym(self):
        self.mtx = self.mtx + self.mtx.transpose()

    def permute(self,nthreads=1):
        mtx_copy = np.copy(self.mtx)
        self.remove_unmappable()
        self.sym2triu()
        i,j = np.indices(self.mtx.shape)
        N = len(self.mtx)
        outdata = np.zeros((N,N), dtype=np.double)
        '''
        #####################################################################################
        #           NOTE ON RANDOMIZATION FUNCTION RUNNING CONCURENTLY IN C++               #
        #####################################################################################
        Currently its done in a bit obscure manner, i.e. function permute_mtx from
        Permutation.so (which is called in every thread to randomize matrices) creates
        its own pseudo random number generator with a seed provided as an argument. Therefore
        we need to provide unique seeds otherwise we will end up with the same matrices
        between 2 or more threads. The seed range is limited by unsigned int range, so unless
        we want to generate more than unsigned int range number of permuted matrices (which
        is unlikely) we should be fine.
        '''
        RR = 4294967295
        random_seed = randrange(0,RR)
        self.__fun(nthreads,random_seed,N,self.mtx,outdata)
        self.mtx = outdata
        self.triu2sym()
        self.restore_unmappable()
        m = self.mtx
        self.mtx = mtx_copy
        return m

######################################################
######################################################

def randomize(queue,domains,masked_diagonals,foo_tstat,tmpdir,
        maps=None,path=''):
    # if maps is None than path to permuted maps have to be specified
    # otherwise maps object need to be given
    while True:
        task = queue.get()
        if task is None:
            queue.task_done()
            break
        if maps is not None:
            permuted = {str(c) : maps[c].permute() for c in maps}
            if path:
                np.savez('{0}/permuted-maps-{1}.npz'.format(path,task),**permuted)
        else:
            permuted = np.load('{0}/permuted-maps-{1}.npz'.format(path,task))
        # calculate test statistic
        tstat_distr = {}
        for id in domains:
            c,s,e = domains[id]
            if e-s > len(masked_diagonals):
                mtx = permuted[str(c)][s:e,s:e]
                vals = []
                for i in range(len(mtx)):
                    if i not in masked_diagonals:
                        vals.extend(mtx.diagonal(offset=i))
                tstat_distr[id] = foo_tstat(vals)
        # save results
        np.savez('{0}/tstat-permuted-maps-{1}.npz'.format(tmpdir,task),
                **{str(k) : v for k,v in tstat_distr.iteritems()})
        queue.task_done()

class Maps(object):

    def __init__(self,directory1,directory2,masked_diagonals=[], normalize=None):
        '''
        directory with contact maps in numpy format - this can be differential
        contact maps OR contact maps, in which case directory and directory2 shouldbe provided
        THIS DIRECTORY MUST CONTAIN ONLY FILES WITH MAPS IN NUMPY FORMAT. ALL FILES NEED TO
        HAVE NUMBER IN THEIR FILE NAME INDICATING CHROMOSOME AND NO OTHER NUMBERS.
        EXAMPLES OF CORRECT FILE NAMES ARE: mtx-1-1.npy (chromosome 1), mtx-11.npy (chromosome 11),
        11.npy (chromosome 11), 11 (chromosome 11), mtx-11 (chromosome 11), mtx-1-1 (chromosome 1), etc.
        '''
        def filenames(d):
            cf = {}
            for f in os.listdir(d):
                c = set(re.findall(r'[1-9][0-9]*',f))
                if len(c) == 1:
                    cf[int(c.pop())] = np.load('{0}/{1}'.format(d,f))
            return cf
        ###################################################################
        m1 = filenames(directory1)
        m2 = filenames(directory2)
        chromosomes = set(m1.keys()) & set(m2.keys())
        self.maps = {}
        for c in chromosomes:
            mtx1 = MtxWrapper(mtx=m1[c],masked_diagonals=masked_diagonals)
            mtx2 = MtxWrapper(mtx=m2[c],masked_diagonals=masked_diagonals)
            if normalize is not None:
                mtx1.normalize(constant=normalize)
                mtx2.normalize(constant=normalize)
            else:
                avg = 1.0 * (mtx1.sum() + mtx2.sum()) / 2
                mtx1.normalize(constant=avg)
                mtx2.normalize(constant=avg)
            self.maps[c] = mtx1 - mtx2
        if len(self.maps) != len(m1) or len(self.maps) != len(m2):
            warnings.warn("The number of chromosomes in {0} is different than in {1}!".format(directory1,directory2))
        self.masked_diagonals = masked_diagonals

    def __getitem__(self, key):
        return self.maps[key]

    def __setitem__(self, key, value):
        # key is id, value is chromosome, start, end
        self.maps[int(key)] = value

    def __iter__(self):
        return iter(self.maps)

    def __len__(self):
        return len(self.maps)

    def randomize_maps(self,domains,foo_tstat,iterations=10,nprocess=22,save_dir='.',path='',load=True):
        tmpdir = tempfile.mkdtemp()
        queue = JoinableQueue()
        for _ in range(nprocess):
            p = Process(target=randomize,
                    args=(queue,domains,self.masked_diagonals,foo_tstat,tmpdir),
                    kwargs={'maps' : None if load and path else self, 'path' : path})
            p.start()
        for i in range(1,iterations+1):
            queue.put(i)
        for _ in range(nprocess):
            queue.put(None)
        queue.join()
        permuted_tstat_distr = defaultdict(list)
        for i in range(1,iterations+1):
            m = np.load('{0}/tstat-permuted-maps-{1}.npz'.format(tmpdir,i))
            for k in m.keys():
                permuted_tstat_distr[k].append(m[k])
        np.savez('{0}/permuted-tstat-{1}-distribution.npz'.format(save_dir,foo_tstat.__name__),**permuted_tstat_distr)
        shutil.rmtree(tmpdir)

######################################################
######################################################

class Domains(object):

    def __init__(self,domains=None):
        if domains is not None:
            self.domains = domains
        else:
            self.domains = {}
        
    def __getitem__(self, key):
        return self.domains[key]

    def __setitem__(self, key, value):
        # ikey is id, value is chromosome, start, end
        chroms = set(re.findall('[1-9][0-9]*',value[0]))
        assert len(chroms) == 1 , "Can't parse chromosome name!"
        self.domains[key] = map(int,[chroms.pop(),value[1],value[2]])

    def __iter__(self):
        return iter(self.domains)

    def __len__(self):
        return len(self.domains)

    def select_chromosomes(self,*chromosomes):
        c = set(chromosomes)
        return Domains(domains={k : v for k,v in self.domains.iteritems() if v[0] in c})

class Detector(object):

    def __init__(self,options):
        # get maps
        self.options = options
        self.maps = Maps(options.path_maps1,self.options.path_maps2,
                masked_diagonals=self.options.masked_diagonals,normalize=self.options.normalize)
        # parse domains
        self.domains = Domains()
        with open(self.options.path_domains,'r') as handle:
            # remove header
            handle.next()
            for i,line in enumerate(handle):
                l = line.split(self.options.domains_col_sep)
                assert len(l) == 4 , "TADs file lines must contain exactly 4 columns (TADid TADchromosome TADstart TADend)!"
                if self.options.resolution is not None:
                    l[-2] = int(int(l[-2])/self.options.resolution)
                    l[-1] = int(int(l[-1])/self.options.resolution)
                self.domains[int(l[0])] = l[1:]

    def evaluate_domains(self,method=['PB','permutation'][0]):
        if method == 'PB':
            return self.__evaluate_PB()
        elif method == 'permutation':
            return self.__evaluate_permutation()
        else:
            raise Exception("{0} - no such method! Possible methods are: PB, permutation.".format(method))

    def __evaluate_PB(self):
        def PB(mtx,s,e,c):
            #domain = np.array(np.nan_to_num(mtx.mtx[s:e,s:e]))
            domain = mtx.mtx[s:e,s:e]
            if mtx.masked_diagonals:
                diags = range(max(mtx.masked_diagonals)+1,len(domain))
            else:
                diags = range(len(domain))
            p_vector = []
            sum_all = 0.0
            sum_abs = 0.0
            N = 0
            for i in diags:
                diag = domain.diagonal(offset=i)
                N += len(diag)
                sum_all += diag.sum()
                sum_abs += np.abs(diag).sum()
                # p_vector stores both depleted and enriched probs
                p_vector.extend([self.__exp_p[c][i]] * len(diag))
            if sum_all == 0.0:
                return [0.0,1.0,'depleted']
            ppoibin = robjects.r['ppoibin']
            exp_p_depleted, exp_p_enriched = zip(*p_vector)
            # if sum_all is less than zero than domain may be depleted
            # if sum_all is greater than zero than domain may be enriched
            # if sum_all is zero than domain is neither depleted nor enriched
            if sum_all < 0.0:
                rvector = robjects.FloatVector(exp_p_depleted)
            elif sum_all > 0.0:
                rvector = robjects.FloatVector(exp_p_enriched)
            n_observed = int(round(abs(sum_all) / sum_abs * N,0))
            cdf = ppoibin(n_observed,pp=rvector)
            pval = 1. - list(cdf)[0]
            return [n_observed,pval, 'enriched' if sum_all > 0.0 else 'depleted']

        robjects.r('install.packages("poibin")')
        robjects.r('library(poibin)')
        # Now we need to calculate expected probabilities for depleted and enriched interaction for each diagonal in each chromosome
        self.__exp_p = defaultdict(list)
        # in above dict keys are pairs of chromosomes and values are lists. list index corresponds to diagonal index and on each index there is a tuple: (p_depleted,p_enriched)
        for c in self.maps:
            mtx = self.maps[c]
            if mtx.masked_diagonals:
                rng = range(max(mtx.masked_diagonals)+1,len(mtx.mtx))
            else:
                rng = range(len(mtx.mtx))
            for i in rng:
                diag = np.nan_to_num(mtx.mtx.diagonal(offset=i))
                N = len(diag)
                p_depleted = 1.0 * len(diag[diag < 0.0]) / N
                p_enriched = 1.0 * len(diag[diag > 0.0]) / N
                self.__exp_p[c].append((p_depleted,p_enriched))

        result = {}
        for c in self.maps:
            mtx = self.maps[c]
            doms = self.domains.select_chromosomes(c)
            for id in doms:
                c, s, e = doms[id]
                result[id] = [c,s,e] + PB(mtx,s,e,c)
        return result

    def __evaluate_permutation(self):
        def cut_map(s,e,mtx):
            m = mtx[s:e,s:e]
            s = set(self.options.masked_diagonals)
            l = []
            for i in range(len(m)):
                if i not in s:
                    l.extend(m.diagonal(offset=i))
            m = l
            return m
        if os.path.isfile(self.options.permuted_tstat_file):
            tstat_distr = np.load(self.options.permuted_tstat_file)
        else:
            load = False
            p = ''
            if self.options.permut_dir:
                p = '{0}/{1}'.format(self.options.out_dir,self.options.permut_dir)
                if os.path.exists(p):
                    npermut_pdir = set(re.findall(r'\d+'," ".join(os.listdir(p))))
                    if npermut_pdir == set(map(str,range(1,self.options.permutations+1))):
                        load = True
                else:
                    os.makedirs(p)
            self.maps.randomize_maps(self.domains,self.options.foo_tstat,
                    iterations=self.options.permutations,
                    nprocess=self.options.nprocess,
                    save_dir=self.options.out_dir,
                    path=p,load=load)
            fname = '{0}/permuted-tstat-{1}-distribution.npz'.format(self.options.out_dir,self.options.foo_tstat.__name__)
            tstat_distr = np.load(fname)
            if self.options.permuted_tstat_file:
                os.rename(fname,self.options.permuted_tstat_file)

        # Now random distribution of test statistic is calculated, so evaluate domains
        results = {}
        for id in self.domains:
            c, s, e = self.domains[id]
            if e - s > len(self.options.masked_diagonals):
                tstat_distr_dom = tstat_distr[str(id)]
                mean_random_ts = np.mean(tstat_distr_dom)
                ts = self.options.foo_tstat(cut_map(s,e,self.maps[c].mtx))
                if ts >= mean_random_ts:
                    # test for enrichment
                    pval = 1.0 * len(tstat_distr_dom[tstat_distr_dom >= ts]) / len(tstat_distr_dom)
                    results[id] = [c,s,e,ts,pval,'enriched']
                else:
                    # test for depletion
                    pval = 1.0 * len(tstat_distr_dom[tstat_distr_dom <= ts]) / len(tstat_distr_dom)
                    results[id] = [c,s,e,ts,pval,'depleted']
        return results

######################################################
######################################################

class Options(object):

    def __init__(self):
        # general options
        self.path_maps1 = cnf.PATH_MAPS1
        assert os.path.isdir(self.path_maps1) , "PATH_MAPS1 directory do not exists!"
        self.path_maps2 = cnf.PATH_MAPS2
        assert os.path.isdir(self.path_maps2) , "PATH_MAPS2 directory do not exists!"
        self.normalize = cnf.NORMALIZE
        if self.normalize is not None:
            assert self.normalize >= 1.0 , "NORMALIZE have to be >= 1.0!"
        self.masked_diagonals = cnf.MASKED_DIAGONALS
        if self.masked_diagonals:
            try:
                map(int,self.masked_diagonals)
            except ValueError:
                sys.exit('MASKED_DIAGONALS must be empty or contain only non negative integer values!')
            assert all(i >= 0 for i in self.masked_diagonals) , 'masked_diagonals must contain only non negative integer values!'
        self.path_domains = cnf.PATH_DOMAINS
        assert os.path.exists(self.path_domains) , 'PATH_DOMAINS file do not exists!'
        self.resolution = cnf.CONVERT_RESOLUTION
        if self.resolution is not None:
            try:
                int(self.resolution)
            except ValueError:
                sys.exit('CONVERT_RESOLUTION must be positive integer value!')
            if self.resolution <= 0:
                sys.exit('CONVERT_RESOLUTION must be positive integer value!')
        self.domains_col_sep = cnf.DOMAINS_COL_SEP
        self.nprocess = cnf.NPROCESS
        try:
            int(self.nprocess)
        except ValueError:
            sys.exit('NPROCESS must be positive integer value!')
        if self.nprocess <= 0:
            sys.exit('NPROCESS must be positive integer value!')
        self.method = cnf.m
        assert self.method in ['PB','permutation'] , 'METHOD not recognized! Available methods are:  0 (parametric), 1 (permutation).'
        self.pval = cnf.PVAL_THRESHOLD
        if self.pval is not None:
            try:
                float(self.pval)
            except ValueError:
                sys.exit('PVAL_THRESHOLD must be float!')
            if not (0.0 <= self.pval <= 1.0):
                sys.exit('Pval_threshold must be in [0,1]!')
        self.mt_cor = cnf.MULTIPLE_TESTING_CORRECTION
        assert self.mt_cor in [True,False] , 'MULTIPLE_TESTING_CORRECTION can be either True or False!'
        if self.mt_cor:
            assert self.pval is not None , 'MULTIPLE_TESTING_CORRECTION is True, but PVAL_THRESHOLD not specified! Please specify PVAL_THRESHOLD!'
        # options relevant for permutation method
        if self.method == 'permutation':
            self.permutations = cnf.PERMUTATIONS
            try:
                int(self.permutations)
            except ValueError:
                sys.exit('PERMUTATIONS must be positive integer value!')
            if self.nprocess <= 0:
                sys.exit('PERMUTATIONS must be positive integer value!')
            self.permut_dir = cnf.PERMUTATIONS_DIRECTORY
            self.permuted_tstat_file = cnf.PERMUTED_TSTAT_FILE
        self.out_dir = cnf.OUT_DIR
        if os.path.exists(self.out_dir):
            if not os.path.isdir(self.out_dir):
                sys.exit('Path specified as OUT_DIR ({0}) is existing file! Specify another path!'.format(self.out_dir))
            else:
                if os.path.exists('{0}/differential_domains.txt'.format(self.out_dir)):
                    sys.exit('Specified directory contains file differential_domains.txt, executing script would overwrite this file! Please specify another path!')
        else:
            os.makedirs(self.out_dir)
            print 'Created directory {0}'.format(self.out_dir)

######################################################
######################################################

if __name__=="__main__":
    op = Options()
    ########## test statistic for permutation method ##########
    op.foo_tstat = np.median
    ########## test statistic for permutation method ##########
    detector = Detector(op)
    diff_domains = detector.evaluate_domains(method=op.method)

    if op.mt_cor:
        l = np.array([[k] + v for k,v in diff_domains.iteritems()])
        reject, pcorr, alphacSidak, alphacBonf = multipletests(map(float,l[:,5]),alpha=op.pval,method='bonferroni')
        diff_domains = {v[0] : v[1:] for v in l[reject,:]}
    elif op.pval is not None:
        diff_domains = {k : v for k,v in diff_domains.iteritems() if float(v[5]) < op.pval}

    with open('{0}/differential_domains.txt'.format(op.out_dir),'w') as handle:
        handle.write('domain_id chromosome start end tstat pvalue domain_type')
        for k,v in diff_domains.iteritems():
            handle.write('\n{0} {1} {2} {3} {4} {5} {6}'.format(k,v[0],v[1],v[2],v[3],v[4],v[5]))
